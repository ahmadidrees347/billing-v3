package com.demo.billing.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AugmentedSkuDetails(
    val canPurchase: Boolean,
    @PrimaryKey val sku: String,
    val type: String?,
    val price: String?,
    val title: String?,
    val description: String?,
    val originalJson: String?,
    val introductoryPrice: String?,
    val freeTrialPeriod: String?,
    val priceCurrencyCode: String?
)