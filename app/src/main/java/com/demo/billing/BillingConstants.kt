package com.demo.billing

object BillingConstants {
    const val KEY_IS_PURCHASED = "KEY_IS_PURCHASED"

    const val MonthlyPackage: String = "android.test.purchased"
    const val subscriptionIdTest : String = "android.test.purchased"

    val subscriptionPackages = listOf(
        MonthlyPackage
    )
    val NON_CONSUMABLE_SKUS = subscriptionPackages
}