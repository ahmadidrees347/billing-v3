package com.demo.billing.app

import com.demo.billing.repository.BillingRepository
import com.demo.billing.viewmodel.BillingViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object AppModule {

    val getModule = module {
        single { BillingRepository(application = get()) }
        viewModel { BillingViewModel(application = get()) }
    }
}