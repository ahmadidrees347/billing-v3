package com.demo.billing

import android.content.Context
import com.demo.billing.utils.SharedPreferenceData


fun Context.isAlreadyPurchased(): Boolean {
    return SharedPreferenceData.getBoolean(
        this,
        BillingConstants.KEY_IS_PURCHASED,
        false
    )
}
