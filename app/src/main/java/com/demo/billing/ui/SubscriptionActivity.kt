package com.demo.billing.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.demo.billing.BillingConstants
import com.demo.billing.BuildConfig
import com.demo.billing.databinding.ActivitySubscriptionBinding
import com.demo.billing.utils.SharedPreferenceData

class SubscriptionActivity : BaseClass() {

    private lateinit var binding: ActivitySubscriptionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySubscriptionBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpViews()
        setupObserver()
    }

    private fun setupObserver() {
        billingViewModel.subSkuDetailsListLiveData.observe(
            this,
            { skuList ->
                skuList.forEachIndexed { _, augmentedSkuDetails ->
                    Log.d("Subscription_Event", "${augmentedSkuDetails.title}")

                    if (augmentedSkuDetails.sku == BillingConstants.MonthlyPackage) {
                        binding.txtMonthlyPrice.text = augmentedSkuDetails.price
                        if (!augmentedSkuDetails.canPurchase) {
                            SharedPreferenceData.putBoolean(
                                this,
                                BillingConstants.KEY_IS_PURCHASED,
                                true
                            )
                            Toast.makeText(this@SubscriptionActivity, "SuccessFully Purchased", Toast.LENGTH_SHORT).show()
                            finishAffinity()
                            startActivity(Intent(this@SubscriptionActivity, MainActivity::class.java))
                        }
                    }
                }
            })
    }

    private fun setUpViews() {
        with(binding) {
            imgClose.setOnClickListener {
                finish()
            }
            subButton.setOnClickListener {
                val packageKey =
                    when {
                        BuildConfig.DEBUG -> BillingConstants.subscriptionIdTest
                        else -> BillingConstants.MonthlyPackage
                    }

                billingViewModel.getBySkuId(packageKey)?.let {
                    billingViewModel.makePurchase(this@SubscriptionActivity, it)
                }
            }
        }
    }
}