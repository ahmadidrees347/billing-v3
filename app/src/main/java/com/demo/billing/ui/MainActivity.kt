package com.demo.billing.ui

import android.content.Intent
import android.os.Bundle
import com.demo.billing.BillingConstants
import com.demo.billing.databinding.ActivityMainBinding
import com.demo.billing.utils.SharedPreferenceData

class MainActivity : BaseClass() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupObserver()
        setUpView()
    }

    private fun setupObserver() {
        billingViewModel.subSkuDetailsListLiveData.observe(this,
            { skuList ->
                skuList.forEachIndexed { _, augmentedSkuDetails ->
                    if (augmentedSkuDetails.sku == BillingConstants.MonthlyPackage) {
                        if (!augmentedSkuDetails.canPurchase) {
                            SharedPreferenceData.putBoolean(
                                this,
                                BillingConstants.KEY_IS_PURCHASED,
                                true
                            )
                        } else {
                            SharedPreferenceData.putBoolean(
                                this,
                                BillingConstants.KEY_IS_PURCHASED,
                                false
                            )
                        }
                    }
                }
            })
    }

    private fun setUpView() {
        with(binding) {
            btnStart.setOnClickListener {
                startActivity(Intent(this@MainActivity, SubscriptionActivity::class.java))
            }
        }
    }
}