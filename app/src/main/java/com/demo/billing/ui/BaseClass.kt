package com.demo.billing.ui

import androidx.appcompat.app.AppCompatActivity
import com.demo.billing.viewmodel.BillingViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

open class BaseClass : AppCompatActivity() {

    val billingViewModel: BillingViewModel by viewModel()
}