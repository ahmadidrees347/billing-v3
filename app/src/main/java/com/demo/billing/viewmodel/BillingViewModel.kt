package com.demo.billing.viewmodel

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.demo.billing.repository.BillingRepository
import com.demo.billing.model.AugmentedSkuDetails

class BillingViewModel(application: Application) : AndroidViewModel(application) {

    private val tag = "BillingViewModel"
    var subSkuDetailsListLiveData: LiveData<List<AugmentedSkuDetails>>

    var billingRepository = BillingRepository.getInstance(application)
    init {
        billingRepository.startDataSourceConnections()
        subSkuDetailsListLiveData = billingRepository.subSkuDetailsListLiveData
    }

    fun getBySkuId(skuId: String): AugmentedSkuDetails?{

        if (subSkuDetailsListLiveData.value != null)
        for (item in subSkuDetailsListLiveData.value!!){
            if (item.sku == skuId) {
                return item
            }
        }
        return null
    }

    fun queryPurchases() = billingRepository.queryPurchasesAsync()

    override fun onCleared() {
        super.onCleared()
        Log.d(tag, "onCleared")
//        billingRepository.endDataSourceConnections()
//        viewModelScope.coroutineContext.cancel()
    }

    fun makePurchase(activity: Activity, augmentedSkuDetails: AugmentedSkuDetails) {
        billingRepository.launchBillingFlow(activity, augmentedSkuDetails)
    }

}